package net.example.preview;

import java.util.Scanner;

public class Example1 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String input = sc.nextLine();

        String[] split = input.split(" ");
        System.out.println(Long.parseLong(split[0]) + Long.parseLong(split[1]));
    }

}
