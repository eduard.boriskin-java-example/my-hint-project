package net.example.preview;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Example3 {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        String input = sc.nextLine();
        String inputArray = sc.nextLine();

        List<Long> collect =
                Arrays.stream(inputArray.split(" ")).map(Long::valueOf).collect(Collectors.toList());

        System.out.println(Collections.max(collect) - Collections.min(collect));

    }

}
