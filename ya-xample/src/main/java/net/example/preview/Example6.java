package net.example.preview;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Collectors;

public class Example6 {

    public static void main(String[] args) throws IOException {

        StringBuilder in = new StringBuilder();

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String line;
        while ((line = br.readLine()) != null) {
            in.append(line);
        }

        String input = in.toString().replace(" ", "");

        List<Character> chars = input.chars().mapToObj(c -> (char) c).collect(Collectors.toList());
        Map<Character, Integer> map = new TreeMap<>();

        for (char ch: chars) {
            map.merge(ch, 1, Integer::sum);
        }

        Integer max = Collections.max(map.values());
        List<Character> characters = List.of(map.keySet().toArray(new Character[0]));

        for (int i = max; i > 0; i--) {
            StringBuilder result = new StringBuilder();

            for (Character character : characters) {
                if (map.get(character) >= i) {
                    result.append("#");
                } else {
                    result.append(" ");
                }
            }
            System.out.println(result);
        }

        StringBuilder result = new StringBuilder();
        for (Character c: characters) {
            result.append(c);
        }
        System.out.println(result);

    }

}
