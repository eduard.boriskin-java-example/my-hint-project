package net.example.lib.exception.impl;

import lombok.Getter;
import net.example.lib.exception.CoreException;
import org.springframework.http.HttpStatus;

@Getter
public class RestCommunicationException extends CoreException {

    private final HttpStatus statusCode;

    public RestCommunicationException(String str, HttpStatus statusCode) {
        super(str);
        this.statusCode = statusCode;
    }
}
